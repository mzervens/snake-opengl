#version 450

layout (location = 0) in vec4 position;
uniform mat4 transformMatrix;

void main()
{
	if(position.y != 0.5)
	{
		gl_Position = vec4(0.5, 0.5, 0.0, 1.0);
	}
	else
	{
		gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
	}
	//gl_Position = position;//transformMatrix * vec4(position.xyz, 1.0);
	gl_PointSize = 10.0;
}
