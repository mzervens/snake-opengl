#include <iostream>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";
#include "lib\glm\glm\ext.hpp"
#include "ShaderLoader.cpp";
#include <vector>
#include "Point.cpp";
// For rng
#include <cstdlib>
#include <ctime>

using namespace std;

enum MoveDirection
{
	UNSET = -1,
	UP = 0,
	DOWN = 1,
	LEFT = 2,
	RIGHT = 3
};

GLuint shaderProgram;
GLuint drawBuffer;

class Game
{
public:
	vector<vector<Point *>> field;
	int currentLength;
	vector<Point *> snakeBody;
	Point * snakeTail;
	Point * snakeHead;
	MoveDirection currentDirection;
	bool failstate;
	vector<float> bodyDrawPositions;
	int gameWidth;
	int gameHeight;
	int lastUpdateTime;
	int lastRenderTime;
	int gameUpdateSpeed;
	int tileSize;
	glm::mat4 coordTransform;
	Point * food;

	Game()
	{

	}

	Game(int width, int height)
	{
		tileSize = 10;
		field = vector<vector<Point *>>(height / tileSize);
		currentLength = 1;
		currentDirection = MoveDirection::UNSET;
		failstate = false;
		gameHeight = height;
		gameWidth = width;
		coordTransform = glm::ortho(0.0f, 800.0f, 800.0f, 0.0f);
		registerUniforms();
		printMat();

	//	glm::vec4 t = coordTransform * glm::vec4(600.0, 200.0, 0.0, 1.0);
//		cout << t.x << " " << t.y << " " << t.z << " " << t.w << endl;

		gameUpdateSpeed = 35;

		for (int i = 0; i < field.size(); i++)
		{
			field[i] = vector<Point *>(width / tileSize);
			for (int n = 0; n < field[i].size(); n++)
			{
				field[i][n] = new Point(n * 10, i * 10);
			}
		}

		snakeBody = vector<Point *>();
		snakeHead = new Point(400.0f, 400.0f);//Point(width / 2, height / 2);
		//Point test = Point(400.0f, 390.0f);

		snakeBody.push_back(snakeHead);
		//snakeBody.push_back(test);
		
		//snakeTail = &snakeBody[1];
		//snakeTail = &snakeBody[0];
		//snakeHead = &snakeBody[0];
		snakeTail = snakeHead;
	//	snakeTail->parent = snakeHead;

		cout << "tail pos " << snakeTail->x << " " << snakeTail->y << endl;
		//snakeTail->parent->y++;
		cout << "head pos " << snakeHead->x << " " << snakeHead->y << endl;
		//snakeHead->x = snakeHead->x + 100;
		food = NULL;
		generateNewFoodLocation();
		cout << "food location " << food->x << " " << food->y << endl;

		addSnakeBodyToDrawBuffer();

		initDrawBuffer();
		lastRenderTime = 0;
		lastUpdateTime = glutGet(GLUT_ELAPSED_TIME);

	}

	void generateNewFoodLocation()
	{
		if (food != NULL)
		{
			delete food;
		}

		while (true)
		{
			int foodX = randomPosition(1, field[0].size() - 1);
			int foodY = randomPosition(1, field.size() - 1);

			Point * p = field[foodY][foodX];

			bool notColiding = true;

			for (int i = 0; i < snakeBody.size(); i++)
			{
				float x = snakeBody[i]->x;
				float y = snakeBody[i]->y;

				// Exact match
				if (p->x == x && p->y == y)
				{
					notColiding = false;
					break;
				}
			}

			if (notColiding)
			{
				food = new Point(p->x, p->y);
				break;
			}

		}
		

	}

	float randomPosition(int min, int max) 
	{
		srand(time(NULL));
		int randNum = rand() % (max - min + 1) + min;
		return randNum;
	}

	void registerUniforms()
	{
		glUseProgram(shaderProgram);

		GLuint transformMtxUnif = glGetUniformLocation(shaderProgram, "transformMatrix");
		glUniformMatrix4fv(transformMtxUnif, 1, GL_FALSE, glm::value_ptr(coordTransform));

		glUseProgram(0);
	}

	void updateRenderTime()
	{
		lastRenderTime = glutGet(GLUT_ELAPSED_TIME);
	}

	void updateUpdateTime()
	{
		lastUpdateTime = glutGet(GLUT_ELAPSED_TIME);
	}

	void update()
	{
		if (currentDirection == MoveDirection::UNSET)
		{
			return;
		}

		if ((lastRenderTime - lastUpdateTime) < gameUpdateSpeed)
		{
			return;
		}

		float oldTailX = snakeTail->x;
		float oldTailY = snakeTail->y;
		float oldHeadX = snakeHead->x;
		float oldHeadY = snakeHead->y;

		Point * curr = snakeTail;

		bool colision = false;

		// Move the snakes head to a new position
		if (currentDirection == MoveDirection::UP)
		{
			snakeHead->y = snakeHead->y - tileSize;
		}
		else if (currentDirection == MoveDirection::DOWN)
		{
			snakeHead->y = snakeHead->y + tileSize;
		}
		else if (currentDirection == MoveDirection::LEFT)
		{
			snakeHead->x = snakeHead->x - tileSize;
		}
		else if (currentDirection == MoveDirection::RIGHT)
		{
			snakeHead->x = snakeHead->x + tileSize;
		}

		if (headOutOfBounds())
		{
			correctOutOfBounds();
		}

		
		// Move each fragment to its parent position
		while (curr->parent != NULL)
		{
			if (curr->parent == snakeHead)
			{
				curr->x = oldHeadX;
				curr->y = oldHeadY;
			}
			else
			{
				curr->x = curr->parent->x;
				curr->y = curr->parent->y;
			}

			if (curr->x == snakeHead->x && curr->y == snakeHead->y)
			{
				colision = true;
			}

			curr = curr->parent;
		}

		if (ateFood())
		{
			Point * newTail = new Point(oldTailX, oldTailY);
			newTail->parent = snakeTail;

			snakeBody.push_back(newTail);
			snakeTail = newTail;

			generateNewFoodLocation();
			currentLength++;

			if (oldTailX == snakeHead->x && oldTailY == snakeHead->y)
			{
				colision = true;
			}
		}

		addSnakeBodyToDrawBuffer();
		updateDrawBufferData();

		lastUpdateTime = glutGet(GLUT_ELAPSED_TIME);

		if (colision)
		{
			cout << currentLength << endl;
			failstate = true;
		}
	}

	void correctOutOfBounds()
	{
		if (snakeHead->x < 0)
		{
			snakeHead->x = field[0][field[0].size() - 1]->x;
		}
		else if (snakeHead->x > gameWidth)
		{
			snakeHead->x = 0;
		}
		else if (snakeHead->y < 0)
		{
			snakeHead->y = field[field.size() - 1][0]->y;
		}
		else if (snakeHead->y > gameHeight)
		{
			snakeHead->y = 0;
		}
	}

	bool headOutOfBounds()
	{
		bool outOfBounds = false;
		if (snakeHead->x < 0 || snakeHead->x > gameWidth)
		{
			outOfBounds = true;
		}
		else if (snakeHead->y < 0 || snakeHead->y > gameHeight)
		{
			outOfBounds = true;
		}

		return outOfBounds;
	}

	bool ateFood()
	{
		bool ate = false;
		if (snakeHead->x == food->x && snakeHead->y == food->y)
		{
			ate = true;
		}

		return ate;
	}

	void printField()
	{
		for (int i = 0; i < field.size(); i++)
		{
			for (int n = 0; n < field[i].size(); n++)
			{
				cout << field[i][n] << " ";
			}
			cout << endl;
		}

	}

	void handleInput(unsigned char key, int x, int y)
	{
		// Esc key
		if (key == 27)
		{
			glutLeaveMainLoop();
			return;
		}
		else if (key == 'w' || key == 'W')
		{
			if (currentDirection == MoveDirection::DOWN)
			{
				return;
			}
			currentDirection = MoveDirection::UP;
		}
		else if (key == 's' || key == 'S')
		{
			if (currentDirection == MoveDirection::UP)
			{
				return;
			}
			currentDirection = MoveDirection::DOWN;
		}
		else if (key == 'a' || key == 'A')
		{
			if (currentDirection == MoveDirection::RIGHT)
			{
				return;
			}
			currentDirection = MoveDirection::LEFT;
		}
		else if (key == 'd' || key == 'D')
		{
			if (currentDirection == MoveDirection::LEFT)
			{
				return;
			}
			currentDirection = MoveDirection::RIGHT;
		}
	}

	void addSnakeBodyToDrawBuffer()
	{
		bodyDrawPositions = vector<float>((snakeBody.size() * 2) + 2);
		
		int n = 0;
		for (int i = 0; i < snakeBody.size(); i++)
		{
			float x = snakeBody[i]->x;
			float y = snakeBody[i]->y;

			bodyDrawPositions[n] = x;
			n++;
			bodyDrawPositions[n] = y;
			n++;
		}

		bodyDrawPositions[n] = food->x;
		bodyDrawPositions[n + 1] = food->y;

	}

	void initDrawBuffer()
	{
		glGenBuffers(1, &drawBuffer);

		glBindBuffer(GL_ARRAY_BUFFER, drawBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * bodyDrawPositions.size(), &bodyDrawPositions.front(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void updateDrawBufferData()
	{
		glBindBuffer(GL_ARRAY_BUFFER, drawBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)* bodyDrawPositions.size(), &bodyDrawPositions.front(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void printMat()
	{
		cout << coordTransform[0].x << " " << coordTransform[1].x << " " << coordTransform[2].x << " " << coordTransform[3].x << endl;
		cout << coordTransform[0].y << " " << coordTransform[1].y << " " << coordTransform[2].y << " " << coordTransform[3].y << endl;
		cout << coordTransform[0].z << " " << coordTransform[1].z << " " << coordTransform[2].z << " " << coordTransform[3].z << endl;
		cout << coordTransform[0].w << " " << coordTransform[1].w << " " << coordTransform[2].w << " " << coordTransform[3].w << endl;
	}

};
Game * g;



void keyboardInput(unsigned char c, int x, int y)
{
	g->handleInput(c, x, y);
}


void render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glUseProgram(shaderProgram);
	glBindBuffer(GL_ARRAY_BUFFER, drawBuffer);

	// Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_POINTS, 0, g->snakeBody.size() + 1);

	glDisableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);

	
	//cout << g.snakeHead->x << " " << g.snakeHead->y << endl;
	if (g->currentDirection != MoveDirection::UNSET && !g->failstate /*&& g->timeDifference >= g->gameUpdateSpeed*/)
	{
		//cout << "rendered frame with update" << endl;
		g->update();
	}


	
	glutSwapBuffers();
	g->updateRenderTime();
	glutPostRedisplay();
	return;
}

int main(int argc, char * argv[])
{
	int defWidth = 800;
	int defHeight = 800;


	// Initializes GLUT (OpenGL Utility Toolkit)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH | GLUT_STENCIL | GLUT_ACCUM | GLUT_RGBA);
	glutInitWindowSize(defWidth, defHeight);
	glutInitWindowPosition(0, 0);

	GLuint windowHandle = glutCreateWindow("Snake");

	glewInit();
	if (glewIsSupported("GL_VERSION_4_5"))
	{
		cout << " GLEW Version is 4.5\n ";
	}
	else
	{
		cout << "GLEW 4.5 not supported\n ";
	}
	glEnable(GL_PROGRAM_POINT_SIZE);


//	cout << g->snakeHead->x << " " << g->snakeHead->y << endl;

	Core::ShaderLoader shaderLoader = Core::ShaderLoader();
	shaderProgram = shaderLoader.createShaderProgram("../Snake/vertex.vert", "../Snake/fragment.frag");

	g = new Game(defWidth, defHeight);

	glutKeyboardFunc(keyboardInput);
	glutDisplayFunc(render);
	glutMainLoop();

	return 0;
}