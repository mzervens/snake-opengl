#version 450

layout (location = 0) in vec2 position;
uniform mat4 transformMatrix;

void main()
{	
	gl_Position = transformMatrix * vec4(position.xy, 0.0, 1.0);//transformMatrix * vec4(position.xyz, 1.0);
	gl_PointSize = 10.0;
}
