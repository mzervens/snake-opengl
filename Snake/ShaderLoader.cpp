#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "lib\glew\glew.h";
#include "lib\freeglut\freeglut.h";
#include "lib\glm\glm\glm.hpp";

using namespace std;

namespace Core
{

	class ShaderLoader
	{
	private:
		string readShader(string filename)
		{
			string fileData;
			fstream f;

			f.open(filename, ios::in);
			if (!f.good())
			{
				cout << "Can't read file " << filename << endl;
				terminate();
			}
			f.seekg(0, ios::end);
			fileData.resize(f.tellg());
			
			f.seekg(0, ios::beg);
			f.read(&fileData[0], fileData.size());
			f.close();

			return fileData;
		}

		GLuint createShader(GLenum shaderType, string shaderSourcecode, string shaderName)
		{
			int compiled = 0;

			GLuint shader = glCreateShader(shaderType);
			const GLchar * shader_code_ptr = shaderSourcecode.c_str();
			const GLint shader_code_size = shaderSourcecode.size();

			glShaderSource(shader, 1, &shader_code_ptr, &shader_code_size);
			glCompileShader(shader);

			glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

			if (compiled == GL_FALSE)
			{

				int info_log_length = 0;
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
				vector<char> shader_log(info_log_length);
				glGetShaderInfoLog(shader, info_log_length, NULL, &shader_log[0]);
				cout << "ERROR compiling shader: " << shaderName << endl << &shader_log[0] << endl;
				return 0;
			}
			return shader;
		}
	public:
		GLuint createShaderProgram(string vertexShaderFilename, string fragmentShaderFilename)
		{
			string vertexShaderCode = readShader(vertexShaderFilename);
			string fragmentShaderCode = readShader(fragmentShaderFilename);

			GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexShaderCode, vertexShaderFilename);
			GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentShaderCode, fragmentShaderFilename);

			int linked = 0;
			GLuint shaderProgram = glCreateProgram();
			glAttachShader(shaderProgram, vertexShader);
			glAttachShader(shaderProgram, fragmentShader);

			glLinkProgram(shaderProgram);

			glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);
			//check for link errors
			if (linked == GL_FALSE)
			{

				int info_log_length = 0;
				glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &info_log_length);
				vector<char> program_log(info_log_length);
				glGetProgramInfoLog(shaderProgram, info_log_length, NULL, &program_log[0]);
				cout << "Shader Loader : LINK ERROR" << endl << &program_log[0] << endl;
				return 0;
			}
			return shaderProgram;
		}


	};
}